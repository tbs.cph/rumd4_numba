from setuptools import setup

setup(name='rumd4_numba',
      version='0.14',
      description='Molecular Dynamics in python and numba',
      url='gitlab.com/tbs.cph/rumd4_numba',
      author='Thomas Schrøder',
      author_email='tbs@ruc.dk',
      license='MIT',
      packages=['rumd4_numba'],
      zip_safe=False, 
      install_requires=[
       "numpy",
       "numba",
       "sympy", 
       "pandas",]
,)
      
      