#import time
#import gzip
import numba
from numba import cuda
import numpy as np
import pandas as pd
import sympy as sp
from sympy.abc import r, A, B, C
from sympy.utilities.lambdify import lambdify


def PairPotentialCalculatorFunc(r, dr, params, uf, duf, dduf):
    return uf(r, params)/2.0

def PairVirialCalculatorFunc(r, dr, params, uf, duf, dduf):
    return -duf(r, params)*r/6.0

def PairLaplaceCalculatorFunc(r, dr, params, uf, duf, dduf):
    return dduf(r, params) + 2.0*duf(r, params)/r

        
class PairPot:
    """ Pair potential from sympy function """

    def __init__(self, ufunc, cutmethod, pnames, pvals, cutoffs, skin):

        self.cutoffs = np.array(cutoffs)
        #self.nblist = NBlist_NSq(cutoffs, skin)
        self.nblist = NBlist_N(cutoffs, skin)

        # First we add terms needed for the cut-off
        # (A,B,C should be made rumd-specific to not mess with user parameters)
        if cutmethod == 'SP':         # Shifted Potential
            ufunc = ufunc - A
            pnames = (*pnames, A)
        if cutmethod == 'SF':         # Shifted Force
            ufunc = ufunc - A - B*r
            pnames = (*pnames, A, B)
        if cutmethod == 'DF':         # Differentiabel Force
            ufunc = ufunc - A - B*r - C*r**2
            pnames = (*pnames, A, B, C)
        if cutmethod == 'smooth_2_4': # Differentiabel Force
            ufunc = ufunc - A - B*r**2 - C*r**4
            pnames = (*pnames, A, B, C)

        # Differentiate using sympy, resulting in sympy functions
        ufunc = sp.simplify(ufunc)
        dufunc = sp.simplify(sp.diff(ufunc, r))
        duOr = sp.simplify(dufunc/r)
        duOr_rsq = sp.simplify(duOr.subs(r, sp.sqrt(r)))
        ddufunc = sp.diff(dufunc, r)

        print('\n*** Functions ***:')
        print('ufunc: ', ufunc)
        print('dufunc: ', dufunc)
        print('ddufunc: ', ddufunc)
        print('duOr_rsq:', duOr_rsq)

        # Make the numerical functions and jit them
        self.lam_u = (lambdify([r, pnames], ufunc, 'numpy'))
        self.lam_du = (lambdify([r, pnames], dufunc, 'numpy'))
        self.lam_duOr_rsq = (lambdify([r, pnames], duOr_rsq, 'numpy'))
        self.lam_ddu = (lambdify([r, pnames], ddufunc, 'numpy'))

        # Update interaction parameters according to cut-off method
        if  cutmethod == 'NO':
            self.pvals = pvals
        if cutmethod == 'SP':
            pvals0 = (*pvals, 0)
            a_val = self.lam_u(cutoffs, pvals0)
            self.pvals = (*pvals, a_val)
        if cutmethod == 'SF':
            pvals0 = (*pvals, 0, 0)
            b_val = self.lam_du(cutoffs, pvals0)
            a_val = self.lam_u(cutoffs, pvals0) - b_val*cutoffs
            self.pvals = (*pvals, a_val, b_val)
        if cutmethod == 'DF':
            pvals0 = (*pvals, 0, 0, 0)
            c_val = self.lam_ddu(cutoffs, pvals0)/2.0
            b_val = self.lam_du(cutoffs, pvals0) - 2.0*c_val*cutoffs
            a_val = self.lam_u(cutoffs, pvals0) - b_val*cutoffs - c_val*cutoffs**2
            self.pvals = (*pvals, a_val, b_val, c_val)
        if cutmethod == 'smooth_2_4':
            pvals0 = (*pvals, 0, 0, 0)
            u_c = self.lam_u(cutoffs, pvals0)
            du_c = self.lam_du(cutoffs, pvals0)
            ddu_c = self.lam_ddu(cutoffs, pvals0)
            c_val = -(  du_c - cutoffs*ddu_c)/8.0*cutoffs**-3
            b_val =  (3*du_c - cutoffs*ddu_c)/4.0*cutoffs**-1
            a_val = u_c - b_val*cutoffs**2 - c_val*cutoffs**4
            self.pvals = (*pvals, a_val, b_val, c_val)

        # Convert to numpy array, and make the 2 types axis the first
        self.pvals = np.moveaxis(np.array(self.pvals), 0, -1)
        print('\n*** Parameters ***:')
        print('i , j : [', *pnames, ']')
        for i in range(np.size(self.pvals, 0)):
            for j in range(np.size(self.pvals, 1)):
                print(i, ',', j, ':', self.pvals[i, j])
                  
        self.calc_force_ = make_forcecalc(self)
        self.calc_potential_ = make_scalarcalc(self, PairPotentialCalculatorFunc)
        self.calc_virial_ = make_scalarcalc(self, PairVirialCalculatorFunc)
        self.calc_laplace_ = make_scalarcalc(self, PairLaplaceCalculatorFunc)

        
    def calc_force(self, conf, out_array):
        self.nblist.update(conf)
        self.calc_force_(conf.r_i, conf.types, conf.sim_box,
                             self.nblist.nonbs, self.nblist.list,
                             out_array)

    def calc_potential(self, conf, out_array):
        self.nblist.update(conf)
        self.calc_potential_(conf.r_i, conf.types, conf.sim_box,
                             self.nblist.nonbs, self.nblist.list,
                             out_array)

    def calc_virial(self, conf, out_array):
        self.nblist.update(conf)
        self.calc_virial_(conf.r_i, conf.types, conf.sim_box,
                             self.nblist.nonbs, self.nblist.list,
                             out_array)        

    def calc_laplace(self, conf, out_array):
        self.nblist.update(conf)
        self.calc_laplace_(conf.r_i, conf.types, conf.sim_box,
                             self.nblist.nonbs, self.nblist.list,
                             out_array)        
class Configuration:
    """ N particles in D dimensions """

    def __init__(self, compute_type=np.float32):
        self.compute_type = compute_type
        self.num_part = 0
        self.D = 0
        self.mass = 0
        self.sim_box = 0
        self.types = 0
        self.r_i = 0
        self.im_i = 0
        self.v_i = 0
        self.f_i = 0
        self.u_i = 0
        self.w_i = 0
        self.comment_line = ""

    def read_rumd_configuration(self, filename, confnr=0, verbose=1):
        """ Read RUMD configuration """
        self.D = 3
        if verbose>0:
            print('\nReadRumdConfiguration(', filename, '), Configuration', confnr)

        lines_for_pandas_to_skip = 2
        # Read first line  and decode comment line into a dict
        with gzip.open(filename, 'r') as file:
            self.num_part = int(file.readline()) # Number of particles in first line
            for c in range(confnr):
                lines_for_pandas_to_skip += self.num_part + 2
                for i in range(self.num_part+1):
                    file.readline()
                self.num_part = int(file.readline())
            self.comment_line = file.readline().decode('UTF-8').strip()
            comment_dict = dict(s.split('=') for s in self.comment_line.split(' '))

        # Read masses
        self.mass = np.array((comment_dict['mass'].split(','))).astype(self.compute_type)
        if verbose>0:
            print('Masses:',self.mass)

        # Read simulation box (only RectangularSimulationBox for now)
        sim_box = comment_dict['sim_box'].split(',')
        assert sim_box[0] == 'RectangularSimulationBox'
        self.sim_box = np.array(sim_box[1:]).astype(np.float)
        if verbose>0:
            print('Number density', self.num_part/np.exp(np.sum(np.log(self.sim_box))))

        # Read data columns using pandas
        columns = comment_dict['columns'].split(',')
        if verbose>0:
            print('Found columns:', columns)
        dataframe = pd.read_csv(filename, names=columns, skiprows=lines_for_pandas_to_skip, nrows=self.num_part, delimiter=' ')
        if verbose>1:
            print(dataframe)
        assert self.num_part == dataframe.shape[0]

        # Convert data columns to numpy arrays
        self.types = dataframe['type'].values
        self.r_i = dataframe[['x', 'y', 'z']].values.astype(self.compute_type) # We could determine spatial dimension (1-4) from this
        self.im_i = dataframe[['imx', 'imy', 'imz']].values

        if 'vx' in dataframe:
            self.v_i = dataframe[['vx', 'vy', 'vz']].values.astype(self.compute_type)
        else:
            self.v_i = np.zeros_like(self.r_i,dtype=self.compute_type)
                        
        if 'fx' in dataframe:
            self.f_i = dataframe[['fx', 'fy', 'fz']].values.astype(self.compute_type)
        else:
            self.f_i = np.zeros_like(self.r_i,dtype=self.compute_type)

        if 'pe' in dataframe:
            self.u_i = dataframe['pe'].values.astype(self.compute_type)
        else:
            self.u_i = np.zeros(self.r_i.shape[0], dtype=self.compute_type)

        if 'vir' in dataframe:
            self.w_i = dataframe['vir'].values.astype(self.compute_type)
        else:
            self.w_i = np.zeros(self.r_i.shape[0], dtype=self.compute_type)

        if verbose>0:
            for i in range(np.max(self.types)+1):
                print(f'Fraction of type {i}: {np.sum(self.types==i)/self.num_part}')

        self.masses = np.reshape(self.mass[self.types],(self.num_part,1))
            
        if verbose>0:
            print('Kinetic temperature [kb]: ', np.mean(self.v_i**2*self.masses)) # -3 df

        self.laplace_i = np.zeros_like(self.u_i)

    def write_rumd_configuration(self, filename, writemode='a'):
        """ write configuration in rumd's xyz format """
        with gzip.open(filename, writemode) as file:
            file.write((str(self.num_part)+'\n').encode("utf-8"))
            file.write((self.comment_line+'\n').encode("utf-8"))
            for i in range(self.num_part):
                st  = str(self.types[i]) + ' '
                for d in range(self.D):
                    st += str(self.r_i[i,d]) + ' '
                for d in range(self.D):
                    st += str(self.im_i[i,d]) + ' '
                st += str(self.u_i[i]) + ' '
                st += str(self.w_i[i]) + '\n'
                file.write(st.encode("utf-8"))

    def read_onlyr_configuration(self, filename, verbose=True):
        """ Read configuration with only positions"""
        self.D = 3
        if verbose:
            print('\nReadOnlyRConfiguration(', filename, ').')

        # Read first line  and decode comment line into a dict
        with open(filename, 'r') as file:
            self.num_part = int(file.readline()) # Number of particles in first line
            comment_line = file.readline().strip()

        # Assume masses identical
        self.mass = np.array((1.0, 1.0, 1.0)).astype(self.compute_type)

        # Read simulation box (only RectangularSimulationBox for now)
        self.sim_box = np.array(comment_line.split(' ')).astype(np.float)
        if verbose:
            print('Number density', self.num_part/np.exp(np.sum(np.log(self.sim_box))))

        # Read data columns using pandas
        columns = ('type', 'x', 'y', 'z')
        dataframe = pd.read_csv(filename, names=columns, skiprows=2, delimiter=' ')
        assert self.num_part == dataframe.shape

        # Convert data columns to numpy arrays
        self.types = dataframe['type'].values - 1
        self.r_i = dataframe[['x', 'y', 'z']].values.astype(self.compute_type)

        self.f_i = np.zeros_like(self.r_i,dtype=self.compute_type)
        self.im_i = np.zeros(self.r_i.shape[0], dtype=np.int)
        self.v_i = np.zeros(self.r_i.shape[0], dtype=self.compute_type)
        self.u_i = np.zeros(self.r_i.shape[0], dtype=self.compute_type)
        self.w_i = np.zeros(self.r_i.shape[0], dtype=self.compute_type)
        
        if verbose:
            for i in range(np.max(self.types)+1):
                print(f'Fraction of type {i}: {np.sum(self.types==i)/self.num_part}')

        self.masses = np.reshape(self.mass[self.types],(self.num_part,1)) # mass for each particle
            
        if verbose:
            print('Kinetic temperature [kb]: ', np.mean(self.v_i**2*self.masses)) # -3 df
        self.laplace_i = np.zeros_like(self.u_i)

           
class NBlist_N:
    """ Neighbour list with order N update using linked lists """

    def __init__(self, cut_offs, skin):
        print('Setting up O(N) neighbor list with skin =', skin)
        self.cut_offs = cut_offs
        self.skin = skin
        self.updates=0

        self.r_ref = np.zeros((0, 0))              # N, D
        self.nonbs = np.zeros(0, dtype=np.int)     #
        self.list = np.zeros((0, 0), dtype=np.int) # N, max_nbs

        # linked list
        self.grid = -np.ones((0,0,0), dtype=np.int) #
        self.my_next =  -np.ones((0), dtype=np.int) # N

    def reset(self):
        self.r_ref = np.zeros((0, 0))
        
    @staticmethod 
    @numba.njit(parallel=False)
    def check_update_needed(r_i, r_ref, sim_box, skin):
        num_part, D = r_i.shape
        for i in range(num_part):
            dist_sq = 0.0
            for k in range(D):
                dr_ij = r_i[i, k] - r_ref[i, k]
                dr_ij += (-sim_box[k] if 2*dr_ij>sim_box[k] else
                          (+sim_box[k] if  2*dr_ij<-sim_box[k] else 0.0))
                dist_sq = dist_sq + dr_ij**2
            if 4*dist_sq > skin**2:
                return True
        return False
            
    def update(self, Conf, force_update=False):
        """ update nblist by linked-list method """
        r_i = Conf.r_i
        types = Conf.types
        sim_box = Conf.sim_box
        update_needed = False
        if r_i.shape != self.r_ref.shape:
            update_needed = True   # Size changed so NBlist invalid
            self.nonbs = np.zeros(r_i.shape[0], dtype=np.int)
            self.list = np.zeros((r_i.shape[0], 0), dtype=np.int)
        else:
            update_needed = self.check_update_needed(r_i, self.r_ref, sim_box, self.skin)

        if update_needed or force_update:
            num_part, D = r_i.shape
            max_cutoff_skin = np.max(self.cut_offs)+self.skin
            grid_dimensions = np.floor(sim_box/max_cutoff_skin).astype(np.int)
            assert(min(grid_dimensions>2))
            self.grid = -np.ones(grid_dimensions, dtype=np.int)
            self.my_next =  -np.ones((num_part), dtype=np.int)
            self.insert_particles(self.grid, self.my_next, sim_box, r_i)
            self.r_ref = np.copy(r_i)
            self.update_numba(r_i, types, sim_box, self.nonbs, self.list, self.cut_offs, self.skin, self.grid, self.my_next)            
            self.updates += 1
            if np.max(self.nonbs) > self.list.shape[1]:
                print('\nIncreasing max_nbs in NBlist to', 2*np.max(self.nonbs))
                self.list = np.zeros((self.list.shape[0], 2*np.max(self.nonbs)), dtype=np.int)
                self.update_numba(r_i, types, sim_box, self.nonbs, self.list, self.cut_offs, self.skin, self.grid, self.my_next)

    @staticmethod         # Needed for numba to work
    @numba.njit(parallel=False) # 'parallel=False' to avoid race-conditions when inserting
    def insert_particles(grid, my_next, sim_box, r_i):
        num_part, D = r_i.shape
        grid_shape = np.array(grid.shape)
        for i in range(num_part):
            ix = int(np.floor(r_i[i,0]/(sim_box[0]/grid_shape[0])))
            iy = int(np.floor(r_i[i,1]/(sim_box[1]/grid_shape[1])))
            iz = int(np.floor(r_i[i,2]/(sim_box[2]/grid_shape[2])))
            
            my_next[i] = grid[ix, iy, iz]
            grid[ix, iy, iz] = i
                
    @staticmethod         # Needed for numba to work
    @numba.njit(parallel=True)
    def update_numba(r_i, types, sim_box, nonbs, nblist, cut_offs, skin, grid, my_next):
        """ Update neighbor list using numba """

        num_part, D = r_i.shape
        max_nbs = nblist.shape[1]
        grid_shape = np.array(grid.shape)
        for i in numba.prange(num_part):
        #for i in range(num_part):
            i_type = types[i]
            my_nonbs = 0               # NOTE: PBC relying on -Lx/2 < x < Lx/2 etc
            ix = int(np.floor(r_i[i,0]/(sim_box[0]/grid_shape[0])))
            iy = int(np.floor(r_i[i,1]/(sim_box[1]/grid_shape[1])))
            iz = int(np.floor(r_i[i,2]/(sim_box[2]/grid_shape[2])))
            for otherix in range(ix-1, ix+2):       # upper limit exclusive 
                for otheriy in range(iy-1, iy+2):   
                    for otheriz in range(iz-1, iz+2):    
                        j = grid[otherix, otheriy, otheriz]  #PBC!
                        while(j >= 0):
                            if j != i:
                                j_type = types[j]
                                dx = r_i[i, 0] - r_i[j, 0]
                                dx +=  (-sim_box[0] if 2*dx>sim_box[0] else
                                        (+sim_box[0] if 2*dx<-sim_box[0] else 0.0 ))
                                dist_sq = dx*dx
                                dy = r_i[i, 1] - r_i[j, 1]
                                dy +=  (-sim_box[1] if 2*dy>sim_box[1] else
                                        (+sim_box[1] if 2*dy<-sim_box[1] else 0.0 ))
                                dist_sq += dy*dy
                                dz = r_i[i, 2] - r_i[j, 2]
                                dz +=  (-sim_box[2] if 2*dz>sim_box[2] else
                                        (+sim_box[2] if 2*dz<-sim_box[2] else 0.0 ))
                                dist_sq += dz*dz
                                if dist_sq < (cut_offs[i_type, j_type]+skin)**2:
                                    if my_nonbs < max_nbs:
                                        nblist[i, my_nonbs] = j
                                    my_nonbs += 1
                            j = my_next[j]
            nonbs[i] = my_nonbs

def make_scalarcalc(pair_potential, function):
    """ Make a function that evaluates 'function' using 'pair_potential' """

    uf = numba.njit(pair_potential.lam_u)
    duf = numba.njit(pair_potential.lam_du)
    dduf = numba.njit(pair_potential.lam_ddu)
    #duOrf = numba.njit(pair_potential.lam_duOr)
    vfunction = numba.njit(function)
    para = pair_potential.pvals
    cutoffs = pair_potential.cutoffs

    @numba.njit(parallel=True)
    def potential(r_i, types, sim_box, nonbs, nblist, out_array):
        num_part, D = r_i.shape
        for i in numba.prange(num_part):
        #for i in range(num_part):
            i_type = types[i]
            my_output = 0.0
            drij = np.zeros(D)
            for j_index in range(nonbs[i]):
                j = nblist[i, j_index]
                j_type = types[j]
                dist_sq = 0.0
                for k in range(D):
                    drij[k] = r_i[i, k] - r_i[j, k]
                    drij[k] = drij[k] - np.rint(drij[k]/sim_box[k])*sim_box[k] # MIC
                    dist_sq += drij[k]**2
                if dist_sq < cutoffs[i_type, j_type]**2:
                    rij = np.sqrt(dist_sq)
                    my_output += vfunction(rij, drij, para[i_type, j_type], uf, duf, dduf)
            out_array[i] = my_output
    return potential


def make_forcecalc(pair_potential):
    """ Make a function that returns the force using 'pair_potential' """

    duf = numba.njit(pair_potential.lam_du)
    duOr_rsq =  numba.njit(pair_potential.lam_duOr_rsq)
    para = pair_potential.pvals
    cutoffs = pair_potential.cutoffs

    @numba.njit(parallel=True)
    def forcecalc(r_i, types, sim_box, nonbs, nblist, out_array):
        num_part, D = r_i.shape
        for i in numba.prange(num_part):
        #for i in range(num_part):
            my_fx = 0.0
            my_fy = 0.0
            my_fz = 0.0
            for j_index in range(nonbs[i]):
                j = nblist[i, j_index]
                dx = r_i[i, 0] - r_i[j, 0]
                dx +=  (-sim_box[0] if 2*dx>sim_box[0] else
                        (+sim_box[0] if 2*dx<-sim_box[0] else 0.0 ))
                dist_sq = dx*dx
                dy = r_i[i, 1] - r_i[j, 1]
                dy +=  (-sim_box[1] if 2*dy>sim_box[1] else
                        (+sim_box[1] if 2*dy<-sim_box[1] else 0.0 ))
                dist_sq += dy*dy
                dz = r_i[i, 2] - r_i[j, 2]
                dz +=  (-sim_box[2] if 2*dz>sim_box[2] else
                        (+sim_box[2] if 2*dz<-sim_box[2] else 0.0 ))
                dist_sq += dz*dz
                if dist_sq < cutoffs[types[i], types[j]]**2:
                    #rij = np.sqrt(dist_sq)
                    #my_temp = duf(rij, para[types[i], types[j]])/rij
                    my_temp = duOr_rsq(dist_sq, para[types[i], types[j]])
                    my_fx -= my_temp*dx
                    my_fy -= my_temp*dy
                    my_fz -= my_temp*dz
            out_array[i,0] = my_fx
            out_array[i,1] = my_fy
            out_array[i,2] = my_fz
            
    return forcecalc
       
def IntegrateNVE(Conf, pot, steps, dt, out_steps):
    out_len = steps//out_steps
    out_array = np.zeros((out_len, 7))
    times = np.zeros(5)
    count = 0

    times[0] = Conf.num_part
    total_time_start = time.time()
    for i in range(steps):
    
        time_start = time.time()
        pot.calc_force(Conf, out_array=Conf.f_i)
        times[2] += time.time() - time_start

        time_start = time.time()
        if i%out_steps==0 and count<out_len:
            pot.calc_potential(Conf, out_array=Conf.u_i)
            pot.calc_virial(Conf, out_array=Conf.w_i)
            pot.calc_laplace(Conf, out_array=Conf.laplace_i)
            out_array[count, 0] = np.sum(Conf.u_i)                            # Potential energy
            out_array[count, 1] = np.sum((Conf.v_i + Conf.f_i*dt/2.0)**2)/2   # Kinetic energy
            out_array[count, 2] = out_array[count, 0] + out_array[count, 1]   # Total energy
            out_array[count, 3] = np.sum(Conf.w_i)                            # Virial          
            out_array[count, 4] = np.sum(Conf.f_i**2)                         # Numerator, Config. temp.
            out_array[count, 5] = np.sum(Conf.laplace_i)                      # Denominator: Config. temp.
            out_array[count, 6] = np.sum(Conf.f_i**2)/np.sum(Conf.laplace_i)  # Configurational temperature
            #print( i, out_array[count, :])
            count += 1
        times[3] += time.time() - time_start

        time_start = time.time()
        Conf.v_i += Conf.f_i/Conf.masses*dt        
        Conf.r_i += Conf.v_i*dt
        if i%1==0:
            Conf.v_i -= np.mean(Conf.v_i*Conf.masses, 0)/Conf.masses

        temp = (2*Conf.r_i>Conf.sim_box).astype(np.int32) - (2*Conf.r_i<-Conf.sim_box).astype(np.int32)
        Conf.im_i += temp
        Conf.r_i -= temp*Conf.sim_box
           
        times[4] += time.time() - time_start
    
    times[1] += steps/(time.time() - total_time_start)    
    print('\nTimes [ms]:', times[2:5]/steps*1000)
    print('TPS = ', times[1])
    print('MATS = ', Conf.num_part*times[1]/1e6)
    return out_array, times

@numba.njit()
def rdf(r_1, r_2, sim_box, bins):
    """ Radial distribution function between particles 'r_1' and 'r_2' """

    num_part_1, dim_1 = r_1.shape
    num_part_2, dim_2 = r_2.shape
    assert dim_2 == dim_1

    for i in range(num_part_1):
        dr_ij = r_2 - r_1[i]
        for k in range(dim_1):
            dr_ij[:, k] -= np.rint(dr_ij[:, k]/sim_box[k])*sim_box[k] # MIC
        dr_ij_mag = np.sqrt(np.sum(dr_ij**2, 1))
        dist_i, edges = np.histogram(dr_ij_mag, bins)
        if i == 0:
            dist = dist_i
        else:
            dist += dist_i
        dr =  (edges[1]-edges[0])  
        r = edges[1:-1] + dr/2
        rdf =  dist[1:]/num_part_1/(4*np.pi*r**2)/dr

    return r, rdf

def rdf_old(r_1, r_2, sim_box, bins, weight):
    """ Radial distribution function between particles 'r_1' and 'r_2' """

    num_part_1, dim_1 = r_1.shape
    num_part_2, dim_2 = r_2.shape
    assert dim_2 == dim_1
    
    for i in range(num_part_1):
        dr_ij = r_2 - r_1[i]
        for k in range(dim_1):
            dr_ij[:, k] -= np.rint(dr_ij[:, k]/sim_box[k])*sim_box[k] # MIC
        dr_ij_mag = np.sqrt(np.sum(dr_ij**2, 1))
        dist_i, edges = np.histogram(dr_ij_mag, bins)
        if i == 0:
            dist = dist_i
            ddist = weights[i]*dist_i
        else:
            dist += dist_i
            ddist += weights[i]*dist_i

    return dist/num_part_1, (ddist-np.mean(weights)*dist)/num_part_1, edges

         
         

__version__ = '4.0.14'
DIM = 3
