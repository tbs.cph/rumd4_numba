import numpy as np
import pandas as pd
import time

def NVE(Conf, pot, steps, dt, out_steps, verbose=False):
    out_len = steps//out_steps
    out_array = np.zeros((out_len, 8))
    times = np.zeros(5)
    count = 0

    times[0] = Conf.num_part
    total_time_start = time.time()
    for i in range(steps):
    
        time_start = time.time()
        pot.calc_force(Conf, out_array=Conf.f_i)
        times[2] += time.time() - time_start

        time_start = time.time()
        if i%out_steps==0 and count<out_len:
            pot.calc_potential(Conf, out_array=Conf.u_i)
            pot.calc_virial(Conf, out_array=Conf.w_i)
            pot.calc_laplace(Conf, out_array=Conf.laplace_i)
            out_array[count, 0] = i*dt
            out_array[count, 1] = np.sum(Conf.u_i)                            # Potential energy
            out_array[count, 2] = np.sum((Conf.v_i + Conf.f_i*dt/2.0)**2)/2   # Kinetic energy
            out_array[count, 3] = out_array[count, 1] + out_array[count, 2]   # Total energy
            out_array[count, 4] = np.sum(Conf.w_i)                            # Virial          
            out_array[count, 5] = np.sum(Conf.f_i**2)                         # Numerator, Config. temp.
            out_array[count, 6] = np.sum(Conf.laplace_i)                      # Denominator: Config. temp.
            out_array[count, 7] = np.sum(Conf.f_i**2)/np.sum(Conf.laplace_i)  # Configurational temperature
            count += 1
        times[3] += time.time() - time_start

        time_start = time.time()
        Conf.v_i += Conf.f_i/Conf.masses*dt        
        Conf.r_i += Conf.v_i*dt
        if i%1==0:
            Conf.v_i -= np.mean(Conf.v_i*Conf.masses, 0)/Conf.masses

        temp = (2*Conf.r_i>Conf.sim_box).astype(np.int32) - (2*Conf.r_i<-Conf.sim_box).astype(np.int32)
        Conf.im_i += temp
        Conf.r_i -= temp*Conf.sim_box
           
        times[4] += time.time() - time_start
    
    times[1] += steps/(time.time() - total_time_start)    
    if verbose:
        print('\nTimes [ms]:', times[2:5]/steps*1000)
        print('TPS = ', times[1])
    columns = ['t', 'U', 'K', 'E', 'W', 'Tconf_num', 'Tconf_denom', 'Tconf']
    out_array[:,1:7] /= Conf.num_part
    df = pd.DataFrame(data=out_array, columns=columns, dtype=np.float)
    return df
