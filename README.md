# Rumd4_numba

Code exploring possibility of using python and numba to implement next major version of RUMD (Roskilde University Molecular Dynamics, rumd.org )

## Installation
Install directly from gitlab: 

pip install git+https://gitlab.com/tbs.cph/rumd4_numba.git

## Usage
"Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README."

## Roadmap
- include GPU support via numba

## Authors and acknowledgment

## License
For open source projects, say how it is licensed.
